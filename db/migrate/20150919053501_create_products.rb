class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.integer :type_id
      t.text :description
      t.integer :status_id
      t.integer :phase_id
      t.boolean :is_published, default: false
      t.integer :company_id
      t.string :e_brochure_url
      t.string :slug
      t.timestamps null: false
    end
    add_index :products, :slug, unique: true
    add_index :products, :phase_id
    add_index :products, :company_id
  end
end
