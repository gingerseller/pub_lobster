class RenameBuyerSourcesTypeidColumn < ActiveRecord::Migration
  def change

  	rename_column :buyers, :source_type_id, :sources_type_id

  end
end
