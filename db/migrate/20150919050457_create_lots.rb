class CreateLots < ActiveRecord::Migration
  def change
    create_table :lots do |t|
      t.string :name
      t.text :description
      t.integer :product_id
      t.integer :product_type_id
      t.integer :status_id, default: 1
      t.integer :land_area_square_meter, default: 0
      t.integer :land_area_square_feet, default: 0
      t.integer :extra_land_square_meter, default: 0
      t.integer :extra_land_square_feet, default: 0
      t.integer :premium
      t.integer :extra_land_price, default: 0
      t.integer :selling_price, default: 0
      t.integer :row_key, default: 1
      t.boolean :is_special_unit, default: false
      t.string :slug
      t.timestamps null: false
    end
    add_index :lots, :slug, unique: true
    add_index :lots, :product_id
    add_index :lots, :product_type_id
  end
end
