class CreateSourcesTypes < ActiveRecord::Migration
  def change
    create_table :sources_types do |t|
      t.string :name
      t.integer :product_id

      t.timestamps null: false
    end
    add_index :sources_types, :product_id
  end
end
