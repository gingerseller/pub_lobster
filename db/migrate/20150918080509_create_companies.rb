class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :registration_number
      t.text :address
      t.string :phone_number
      t.string :fax_number
      t.integer :type_id
      t.integer :parent_id
      t.integer :status_id
      t.string :slug
      t.timestamps null: false
    end
    add_index :companies, :slug, unique: true
  end
end
