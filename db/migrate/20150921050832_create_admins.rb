class CreateAdmins < ActiveRecord::Migration
  def change
    create_table :admins do |t|
      t.string :display_name
      t.string :username
      t.string :slug 
      t.timestamps null: false
    end
    add_index :admins, :slug, unique: true
  end
end
