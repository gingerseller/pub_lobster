class CreateReservationCustomizations < ActiveRecord::Migration
  def change
    create_table :reservation_customizations do |t|
      t.integer :product_id
      t.boolean :full_name                      , default: false
      t.boolean :ic_number                      , default: false
      t.boolean :address                        , default: false
      t.boolean :mobile_contact_number          , default: false
      t.boolean :home_contact_number            , default: false
      t.boolean :office_contact_number          , default: false
      t.boolean :email                          , default: false
      t.boolean :gender                         , default: false
      t.boolean :is_bumi_putera                 , default: false
      t.boolean :race                           , default: false
      t.boolean :age                            , default: false
      t.boolean :buyer_second_name              , default: false
      t.boolean :buyer_third_name               , default: false
      t.boolean :second_ic_number               , default: false
      t.boolean :third_ic_number                , default: false
      t.boolean :postcode                       , default: false
      t.boolean :downpayment                    , default: false
      t.boolean :downpayment_percentage         , default: false
      t.boolean :spa                            , default: false
      t.boolean :booking_fee                    , default: false
      t.boolean :reject_reason                  , default: false
      t.boolean :downpayment_type               , default: false
      t.boolean :confirm_date                   , default: false
      t.boolean :sale_date                      , default: false
      t.boolean :selling_price                  , default: false
      t.boolean :region_id                      , default: false
      t.boolean :nationality_id                 , default: false
      t.boolean :sources_type_id                , default: false
      t.boolean :sale_person                    , default: false
      t.boolean :payment_type                   , default: false
      t.boolean :car_park                       , default: false
      t.boolean :cash                           , default: false
      t.boolean :bank_loan                      , default: false
      t.boolean :government_loan                , default: false
      t.boolean :staff_loan                     , default: false
      t.boolean :remark                         , default: false
      t.boolean :full_name_required             , default: false
      t.boolean :ic_number_required             , default: false
      t.boolean :address_required               , default: false
      t.boolean :mobile_contact_number_required , default: false
      t.boolean :home_contact_number_required   , default: false
      t.boolean :office_contact_number_required , default: false
      t.boolean :email_required                 , default: false
      t.boolean :gender_required                , default: false
      t.boolean :is_bumi_putera_required        , default: false
      t.boolean :race_required                  , default: false
      t.boolean :age_required                   , default: false
      t.boolean :buyer_second_name_required     , default: false
      t.boolean :buyer_third_name_required      , default: false
      t.boolean :second_ic_number               , default: false
      t.boolean :third_ic_number                , default: false
      t.boolean :postcode_required              , default: false
      t.boolean :downpayment_required           , default: false
      t.boolean :downpayment_percentage_required, default: false
      t.boolean :spa_required                   , default: false
      t.boolean :booking_fee_required           , default: false
      t.boolean :reject_reason_required         , default: false
      t.boolean :downpayment_type_required      , default: false
      t.boolean :confirm_date_required          , default: false
      t.boolean :sale_date_required             , default: false
      t.boolean :selling_price_required         , default: false
      t.boolean :region_id_required             , default: false
      t.boolean :nationality_id_required        , default: false
      t.boolean :sale_person_required           , default: false
      t.boolean :payment_type_required          , default: false
      t.boolean :car_park_required              , default: false
      t.boolean :cash_required                  , default: false
      t.boolean :bank_loan_required             , default: false
      t.boolean :government_loan_required       , default: false
      t.boolean :staff_loan_required            , default: false
      t.boolean :remark_required                , default: false
      t.boolean :sources_type_id_required       , default: false
      t.boolean :third_ic_number_required       , default: false
      t.boolean :second_ic_number_required      , default: false
      t.timestamps null: false
    end
    add_index :reservation_customizations, :product_id
  end
end
