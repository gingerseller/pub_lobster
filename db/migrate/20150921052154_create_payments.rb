class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :sale_id

      t.timestamps null: false
    end
    add_index :payments, :sale_id
  end
end
