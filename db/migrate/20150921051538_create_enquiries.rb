class CreateEnquiries < ActiveRecord::Migration
  def change
    create_table :enquiries do |t|
      t.integer :product_id

      t.timestamps null: false
    end
    add_index :enquiries, :product_id
  end
end
