class CreateFloorPlans < ActiveRecord::Migration
  def change
    create_table :floor_plans do |t|
      t.integer :product_id

      t.timestamps null: false
    end
    add_index :floor_plans, :product_id
  end
end
