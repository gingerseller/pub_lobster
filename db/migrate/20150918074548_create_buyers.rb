class CreateBuyers < ActiveRecord::Migration
  def change
    create_table :buyers do |t|
      t.string :full_name
      t.string :ic_number
      t.string :address
      t.string :mobile_contact_number
      t.string :home_contact_number
      t.string :email
      t.string :gender
      t.boolean :is_bumi_putera, default: false
      t.integer :source_type_id
      t.integer :race
      t.integer :age
      t.integer :region_id
      t.string :buyer_second_name
      t.string :buyer_third_name
      t.string :second_ic_number
      t.integer :third_ic_number
      t.integer :nationality_id
      t.integer :postcode
      t.string :car_park
      t.text :remark
      t.string :slug
      t.timestamps null: false
    end
    add_index :buyers, :slug, unique: true
    add_index :buyers, :nationality_id
    add_index :buyers, :region_id
  end
end
