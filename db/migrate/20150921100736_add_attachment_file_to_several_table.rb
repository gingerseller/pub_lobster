class AddAttachmentFileToSeveralTable < ActiveRecord::Migration
  def self.up
    change_table :floor_plans do |t|
      t.attachment :image
    end
    change_table :site_plans do |t|
    	t.attachment :image
    end
    change_table :companies do |t|
    	t.attachment :logo
    end	
    change_table :payments do |t|
    	t.attachment :image
    end
  end

  def self.down
    remove_attachment :floor_plans, :image
    remove_attachment :site_plans, :image
    remove_attachment :companies, :logo
    remove_attachment :payments, :image
  end
end
