class CreateProductSettings < ActiveRecord::Migration
  def change
    create_table :product_settings do |t|
      t.integer :product_id
      t.boolean :allow_multiple_booking, default: false
      t.integer :unit_per_row
      t.boolean :notify_buyer_on_sale_confirmation, default: false
      t.boolean :notify_admin_on_sale_confirmation, default: false
      t.integer :bumiputera_discount
      t.boolean :attach_payment_image, default: false

      t.timestamps null: false
    end
    add_index :product_settings, :product_id
  end
  
end
