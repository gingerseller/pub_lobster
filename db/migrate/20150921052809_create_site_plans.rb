class CreateSitePlans < ActiveRecord::Migration
  def change
    create_table :site_plans do |t|
      t.integer :product_id

      t.timestamps null: false
    end
    add_index :site_plans, :product_id
  end
end
