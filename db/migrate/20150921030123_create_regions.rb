class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.string :name
      t.integer :product_id

      t.timestamps null: false
    end
    add_index :regions, :product_id
  end
end
