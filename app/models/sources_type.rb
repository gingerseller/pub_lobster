# == Schema Information
#
# Table name: sources_types
#
#  id         :integer          not null, primary key
#  name       :string
#  product_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SourcesType < ActiveRecord::Base
	has_many :buyers
  belongs_to :product


end
