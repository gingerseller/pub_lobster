require "test_helper"

class SitePlansControllerTest < ActionController::TestCase
  def site_plan
    @site_plan ||= site_plans :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:site_plans)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("SitePlan.count") do
      post :create, site_plan: { product_id: site_plan.product_id }
    end

    assert_redirected_to site_plan_path(assigns(:site_plan))
  end

  def test_show
    get :show, id: site_plan
    assert_response :success
  end

  def test_edit
    get :edit, id: site_plan
    assert_response :success
  end

  def test_update
    put :update, id: site_plan, site_plan: { product_id: site_plan.product_id }
    assert_redirected_to site_plan_path(assigns(:site_plan))
  end

  def test_destroy
    assert_difference("SitePlan.count", -1) do
      delete :destroy, id: site_plan
    end

    assert_redirected_to site_plans_path
  end
end
