require "test_helper"

class ProductSettingsControllerTest < ActionController::TestCase
  def product_setting
    @product_setting ||= product_settings :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:product_settings)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("ProductSetting.count") do
      post :create, product_setting: { allow_multiper_booking: product_setting.allow_multiper_booking, attach_payment_image: product_setting.attach_payment_image, bumiputera_discount: product_setting.bumiputera_discount, notify_admin_on_sale_confirmation: product_setting.notify_admin_on_sale_confirmation, notify_buyer_on_sale_confirmation: product_setting.notify_buyer_on_sale_confirmation, product_id: product_setting.product_id, unit_per_row: product_setting.unit_per_row }
    end

    assert_redirected_to product_setting_path(assigns(:product_setting))
  end

  def test_show
    get :show, id: product_setting
    assert_response :success
  end

  def test_edit
    get :edit, id: product_setting
    assert_response :success
  end

  def test_update
    put :update, id: product_setting, product_setting: { allow_multiper_booking: product_setting.allow_multiper_booking, attach_payment_image: product_setting.attach_payment_image, bumiputera_discount: product_setting.bumiputera_discount, notify_admin_on_sale_confirmation: product_setting.notify_admin_on_sale_confirmation, notify_buyer_on_sale_confirmation: product_setting.notify_buyer_on_sale_confirmation, product_id: product_setting.product_id, unit_per_row: product_setting.unit_per_row }
    assert_redirected_to product_setting_path(assigns(:product_setting))
  end

  def test_destroy
    assert_difference("ProductSetting.count", -1) do
      delete :destroy, id: product_setting
    end

    assert_redirected_to product_settings_path
  end
end
