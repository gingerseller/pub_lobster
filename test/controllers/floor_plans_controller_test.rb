require "test_helper"

class FloorPlansControllerTest < ActionController::TestCase
  def floor_plan
    @floor_plan ||= floor_plans :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:floor_plans)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("FloorPlan.count") do
      post :create, floor_plan: { product_id: floor_plan.product_id }
    end

    assert_redirected_to floor_plan_path(assigns(:floor_plan))
  end

  def test_show
    get :show, id: floor_plan
    assert_response :success
  end

  def test_edit
    get :edit, id: floor_plan
    assert_response :success
  end

  def test_update
    put :update, id: floor_plan, floor_plan: { product_id: floor_plan.product_id }
    assert_redirected_to floor_plan_path(assigns(:floor_plan))
  end

  def test_destroy
    assert_difference("FloorPlan.count", -1) do
      delete :destroy, id: floor_plan
    end

    assert_redirected_to floor_plans_path
  end
end
