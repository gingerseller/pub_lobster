require "test_helper"

class NationalitiesControllerTest < ActionController::TestCase
  def nationality
    @nationality ||= nationalities :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:nationalities)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("Nationality.count") do
      post :create, nationality: { name: nationality.name }
    end

    assert_redirected_to nationality_path(assigns(:nationality))
  end

  def test_show
    get :show, id: nationality
    assert_response :success
  end

  def test_edit
    get :edit, id: nationality
    assert_response :success
  end

  def test_update
    put :update, id: nationality, nationality: { name: nationality.name }
    assert_redirected_to nationality_path(assigns(:nationality))
  end

  def test_destroy
    assert_difference("Nationality.count", -1) do
      delete :destroy, id: nationality
    end

    assert_redirected_to nationalities_path
  end
end
