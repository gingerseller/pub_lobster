require "test_helper"

class EnquiryFieldsControllerTest < ActionController::TestCase
  def enquiry_field
    @enquiry_field ||= enquiry_fields :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:enquiry_fields)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("EnquiryField.count") do
      post :create, enquiry_field: { content: enquiry_field.content, enquiry_id: enquiry_field.enquiry_id, name: enquiry_field.name }
    end

    assert_redirected_to enquiry_field_path(assigns(:enquiry_field))
  end

  def test_show
    get :show, id: enquiry_field
    assert_response :success
  end

  def test_edit
    get :edit, id: enquiry_field
    assert_response :success
  end

  def test_update
    put :update, id: enquiry_field, enquiry_field: { content: enquiry_field.content, enquiry_id: enquiry_field.enquiry_id, name: enquiry_field.name }
    assert_redirected_to enquiry_field_path(assigns(:enquiry_field))
  end

  def test_destroy
    assert_difference("EnquiryField.count", -1) do
      delete :destroy, id: enquiry_field
    end

    assert_redirected_to enquiry_fields_path
  end
end
