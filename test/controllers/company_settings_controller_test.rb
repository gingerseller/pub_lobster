require "test_helper"

class CompanySettingsControllerTest < ActionController::TestCase
  def company_setting
    @company_setting ||= company_settings :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:company_settings)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("CompanySetting.count") do
      post :create, company_setting: { allow_multiple_booking: company_setting.allow_multiple_booking, company_id: company_setting.company_id }
    end

    assert_redirected_to company_setting_path(assigns(:company_setting))
  end

  def test_show
    get :show, id: company_setting
    assert_response :success
  end

  def test_edit
    get :edit, id: company_setting
    assert_response :success
  end

  def test_update
    put :update, id: company_setting, company_setting: { allow_multiple_booking: company_setting.allow_multiple_booking, company_id: company_setting.company_id }
    assert_redirected_to company_setting_path(assigns(:company_setting))
  end

  def test_destroy
    assert_difference("CompanySetting.count", -1) do
      delete :destroy, id: company_setting
    end

    assert_redirected_to company_settings_path
  end
end
