require "test_helper"

class EnquiriesControllerTest < ActionController::TestCase
  def enquiry
    @enquiry ||= enquiries :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:enquiries)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("Enquiry.count") do
      post :create, enquiry: { product_id: enquiry.product_id }
    end

    assert_redirected_to enquiry_path(assigns(:enquiry))
  end

  def test_show
    get :show, id: enquiry
    assert_response :success
  end

  def test_edit
    get :edit, id: enquiry
    assert_response :success
  end

  def test_update
    put :update, id: enquiry, enquiry: { product_id: enquiry.product_id }
    assert_redirected_to enquiry_path(assigns(:enquiry))
  end

  def test_destroy
    assert_difference("Enquiry.count", -1) do
      delete :destroy, id: enquiry
    end

    assert_redirected_to enquiries_path
  end
end
