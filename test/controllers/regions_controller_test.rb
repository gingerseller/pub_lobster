require "test_helper"

class RegionsControllerTest < ActionController::TestCase
  def region
    @region ||= regions :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:regions)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("Region.count") do
      post :create, region: { name: region.name, product_id: region.product_id }
    end

    assert_redirected_to region_path(assigns(:region))
  end

  def test_show
    get :show, id: region
    assert_response :success
  end

  def test_edit
    get :edit, id: region
    assert_response :success
  end

  def test_update
    put :update, id: region, region: { name: region.name, product_id: region.product_id }
    assert_redirected_to region_path(assigns(:region))
  end

  def test_destroy
    assert_difference("Region.count", -1) do
      delete :destroy, id: region
    end

    assert_redirected_to regions_path
  end
end
