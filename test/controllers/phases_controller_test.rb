require "test_helper"

class PhasesControllerTest < ActionController::TestCase
  def phase
    @phase ||= phases :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:phases)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("Phase.count") do
      post :create, phase: { description: phase.description, name: phase.name, project_id: phase.project_id, slug: phase.slug, status_id: phase.status_id }
    end

    assert_redirected_to phase_path(assigns(:phase))
  end

  def test_show
    get :show, id: phase
    assert_response :success
  end

  def test_edit
    get :edit, id: phase
    assert_response :success
  end

  def test_update
    put :update, id: phase, phase: { description: phase.description, name: phase.name, project_id: phase.project_id, slug: phase.slug, status_id: phase.status_id }
    assert_redirected_to phase_path(assigns(:phase))
  end

  def test_destroy
    assert_difference("Phase.count", -1) do
      delete :destroy, id: phase
    end

    assert_redirected_to phases_path
  end
end
