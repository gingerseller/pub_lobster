require "test_helper"

class BuyersControllerTest < ActionController::TestCase
  def buyer
    @buyer ||= buyers :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:buyers)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("Buyer.count") do
      post :create, buyer: { address: buyer.address, age: buyer.age, buyer_second_name: buyer.buyer_second_name, buyer_third_name: buyer.buyer_third_name, car_park: buyer.car_park, email: buyer.email, full_name: buyer.full_name, gender: buyer.gender, home_contact_number: buyer.home_contact_number, ic_number: buyer.ic_number, is_bumi_putera: buyer.is_bumi_putera, mobile_contact_number: buyer.mobile_contact_number, nationality_id: buyer.nationality_id, postcode: buyer.postcode, race: buyer.race, region_id: buyer.region_id, remark: buyer.remark, second_ic_number: buyer.second_ic_number, source_type_id: buyer.source_type_id, third_ic_number: buyer.third_ic_number }
    end

    assert_redirected_to buyer_path(assigns(:buyer))
  end

  def test_show
    get :show, id: buyer
    assert_response :success
  end

  def test_edit
    get :edit, id: buyer
    assert_response :success
  end

  def test_update
    put :update, id: buyer, buyer: { address: buyer.address, age: buyer.age, buyer_second_name: buyer.buyer_second_name, buyer_third_name: buyer.buyer_third_name, car_park: buyer.car_park, email: buyer.email, full_name: buyer.full_name, gender: buyer.gender, home_contact_number: buyer.home_contact_number, ic_number: buyer.ic_number, is_bumi_putera: buyer.is_bumi_putera, mobile_contact_number: buyer.mobile_contact_number, nationality_id: buyer.nationality_id, postcode: buyer.postcode, race: buyer.race, region_id: buyer.region_id, remark: buyer.remark, second_ic_number: buyer.second_ic_number, source_type_id: buyer.source_type_id, third_ic_number: buyer.third_ic_number }
    assert_redirected_to buyer_path(assigns(:buyer))
  end

  def test_destroy
    assert_difference("Buyer.count", -1) do
      delete :destroy, id: buyer
    end

    assert_redirected_to buyers_path
  end
end
