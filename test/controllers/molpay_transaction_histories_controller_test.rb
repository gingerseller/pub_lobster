require "test_helper"

class MolpayTransactionHistoriesControllerTest < ActionController::TestCase
  def molpay_transaction_history
    @molpay_transaction_history ||= molpay_transaction_histories :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:molpay_transaction_histories)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("MolpayTransactionHistory.count") do
      post :create, molpay_transaction_history: { amount: molpay_transaction_history.amount, appcode: molpay_transaction_history.appcode, bill_desc: molpay_transaction_history.bill_desc, bill_email: molpay_transaction_history.bill_email, bill_mobile: molpay_transaction_history.bill_mobile, bill_name: molpay_transaction_history.bill_name, channel: molpay_transaction_history.channel, currency: molpay_transaction_history.currency, domain: molpay_transaction_history.domain, error_code: molpay_transaction_history.error_code, error_desc: molpay_transaction_history.error_desc, order_id: molpay_transaction_history.order_id, paydate: molpay_transaction_history.paydate, sale_id: molpay_transaction_history.sale_id, skey: molpay_transaction_history.skey, status: molpay_transaction_history.status, tran_id: molpay_transaction_history.tran_id }
    end

    assert_redirected_to molpay_transaction_history_path(assigns(:molpay_transaction_history))
  end

  def test_show
    get :show, id: molpay_transaction_history
    assert_response :success
  end

  def test_edit
    get :edit, id: molpay_transaction_history
    assert_response :success
  end

  def test_update
    put :update, id: molpay_transaction_history, molpay_transaction_history: { amount: molpay_transaction_history.amount, appcode: molpay_transaction_history.appcode, bill_desc: molpay_transaction_history.bill_desc, bill_email: molpay_transaction_history.bill_email, bill_mobile: molpay_transaction_history.bill_mobile, bill_name: molpay_transaction_history.bill_name, channel: molpay_transaction_history.channel, currency: molpay_transaction_history.currency, domain: molpay_transaction_history.domain, error_code: molpay_transaction_history.error_code, error_desc: molpay_transaction_history.error_desc, order_id: molpay_transaction_history.order_id, paydate: molpay_transaction_history.paydate, sale_id: molpay_transaction_history.sale_id, skey: molpay_transaction_history.skey, status: molpay_transaction_history.status, tran_id: molpay_transaction_history.tran_id }
    assert_redirected_to molpay_transaction_history_path(assigns(:molpay_transaction_history))
  end

  def test_destroy
    assert_difference("MolpayTransactionHistory.count", -1) do
      delete :destroy, id: molpay_transaction_history
    end

    assert_redirected_to molpay_transaction_histories_path
  end
end
