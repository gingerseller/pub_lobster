require "test_helper"

class PaymentsControllerTest < ActionController::TestCase
  def payment
    @payment ||= payments :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:payments)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("Payment.count") do
      post :create, payment: { sale_id: payment.sale_id }
    end

    assert_redirected_to payment_path(assigns(:payment))
  end

  def test_show
    get :show, id: payment
    assert_response :success
  end

  def test_edit
    get :edit, id: payment
    assert_response :success
  end

  def test_update
    put :update, id: payment, payment: { sale_id: payment.sale_id }
    assert_redirected_to payment_path(assigns(:payment))
  end

  def test_destroy
    assert_difference("Payment.count", -1) do
      delete :destroy, id: payment
    end

    assert_redirected_to payments_path
  end
end
