require "test_helper"

class SourcesTypesControllerTest < ActionController::TestCase
  def sources_type
    @sources_type ||= sources_types :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:sources_types)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("SourcesType.count") do
      post :create, sources_type: { name: sources_type.name, product_id: sources_type.product_id }
    end

    assert_redirected_to sources_type_path(assigns(:sources_type))
  end

  def test_show
    get :show, id: sources_type
    assert_response :success
  end

  def test_edit
    get :edit, id: sources_type
    assert_response :success
  end

  def test_update
    put :update, id: sources_type, sources_type: { name: sources_type.name, product_id: sources_type.product_id }
    assert_redirected_to sources_type_path(assigns(:sources_type))
  end

  def test_destroy
    assert_difference("SourcesType.count", -1) do
      delete :destroy, id: sources_type
    end

    assert_redirected_to sources_types_path
  end
end
