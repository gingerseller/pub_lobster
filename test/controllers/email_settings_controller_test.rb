require "test_helper"

class EmailSettingsControllerTest < ActionController::TestCase
  def email_setting
    @email_setting ||= email_settings :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:email_settings)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("EmailSetting.count") do
      post :create, email_setting: { body: email_setting.body, from: email_setting.from, product_id: email_setting.product_id, subject: email_setting.subject }
    end

    assert_redirected_to email_setting_path(assigns(:email_setting))
  end

  def test_show
    get :show, id: email_setting
    assert_response :success
  end

  def test_edit
    get :edit, id: email_setting
    assert_response :success
  end

  def test_update
    put :update, id: email_setting, email_setting: { body: email_setting.body, from: email_setting.from, product_id: email_setting.product_id, subject: email_setting.subject }
    assert_redirected_to email_setting_path(assigns(:email_setting))
  end

  def test_destroy
    assert_difference("EmailSetting.count", -1) do
      delete :destroy, id: email_setting
    end

    assert_redirected_to email_settings_path
  end
end
