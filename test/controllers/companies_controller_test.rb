require "test_helper"

class CompaniesControllerTest < ActionController::TestCase
  def company
    @company ||= companies :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:companies)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("Company.count") do
      post :create, company: { address: company.address, fax_number: company.fax_number, name: company.name, parent_id: company.parent_id, phone_number: company.phone_number, registration_number: company.registration_number, status_id: company.status_id, type_id: company.type_id }
    end

    assert_redirected_to company_path(assigns(:company))
  end

  def test_show
    get :show, id: company
    assert_response :success
  end

  def test_edit
    get :edit, id: company
    assert_response :success
  end

  def test_update
    put :update, id: company, company: { address: company.address, fax_number: company.fax_number, name: company.name, parent_id: company.parent_id, phone_number: company.phone_number, registration_number: company.registration_number, status_id: company.status_id, type_id: company.type_id }
    assert_redirected_to company_path(assigns(:company))
  end

  def test_destroy
    assert_difference("Company.count", -1) do
      delete :destroy, id: company
    end

    assert_redirected_to companies_path
  end
end
