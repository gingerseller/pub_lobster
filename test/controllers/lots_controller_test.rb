require "test_helper"

class LotsControllerTest < ActionController::TestCase
  def lot
    @lot ||= lots :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:lots)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("Lot.count") do
      post :create, lot: { description: lot.description, extra_land_price: lot.extra_land_price, extra_land_square_feet: lot.extra_land_square_feet, extra_land_square_meter: lot.extra_land_square_meter, is_special_unit: lot.is_special_unit, land_area_square_feet: lot.land_area_square_feet, land_area_square_meter: lot.land_area_square_meter, name: lot.name, premium: lot.premium, product_id: lot.product_id, product_type_id: lot.product_type_id, row_key: lot.row_key, selling_price: lot.selling_price, status_id: lot.status_id }
    end

    assert_redirected_to lot_path(assigns(:lot))
  end

  def test_show
    get :show, id: lot
    assert_response :success
  end

  def test_edit
    get :edit, id: lot
    assert_response :success
  end

  def test_update
    put :update, id: lot, lot: { description: lot.description, extra_land_price: lot.extra_land_price, extra_land_square_feet: lot.extra_land_square_feet, extra_land_square_meter: lot.extra_land_square_meter, is_special_unit: lot.is_special_unit, land_area_square_feet: lot.land_area_square_feet, land_area_square_meter: lot.land_area_square_meter, name: lot.name, premium: lot.premium, product_id: lot.product_id, product_type_id: lot.product_type_id, row_key: lot.row_key, selling_price: lot.selling_price, status_id: lot.status_id }
    assert_redirected_to lot_path(assigns(:lot))
  end

  def test_destroy
    assert_difference("Lot.count", -1) do
      delete :destroy, id: lot
    end

    assert_redirected_to lots_path
  end
end
