require "test_helper"

class SalesControllerTest < ActionController::TestCase
  def sale
    @sale ||= sales :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:sales)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("Sale.count") do
      post :create, sale: { admin_confirm_user_id: sale.admin_confirm_user_id, bank_loan: sale.bank_loan, booking_fee: sale.booking_fee, buyer_id: sale.buyer_id, cash: sale.cash, confirm_date: sale.confirm_date, downpayment: sale.downpayment, downpayment_percentage: sale.downpayment_percentage, downpayment_type: sale.downpayment_type, government_loan: sale.government_loan, lot_unit_id: sale.lot_unit_id, phase_id: sale.phase_id, product_id: sale.product_id, project_id: sale.project_id, reject_reason: sale.reject_reason, spa: sale.spa, staff_loan: sale.staff_loan, status_id: sale.status_id, user_id: sale.user_id }
    end

    assert_redirected_to sale_path(assigns(:sale))
  end

  def test_show
    get :show, id: sale
    assert_response :success
  end

  def test_edit
    get :edit, id: sale
    assert_response :success
  end

  def test_update
    put :update, id: sale, sale: { admin_confirm_user_id: sale.admin_confirm_user_id, bank_loan: sale.bank_loan, booking_fee: sale.booking_fee, buyer_id: sale.buyer_id, cash: sale.cash, confirm_date: sale.confirm_date, downpayment: sale.downpayment, downpayment_percentage: sale.downpayment_percentage, downpayment_type: sale.downpayment_type, government_loan: sale.government_loan, lot_unit_id: sale.lot_unit_id, phase_id: sale.phase_id, product_id: sale.product_id, project_id: sale.project_id, reject_reason: sale.reject_reason, spa: sale.spa, staff_loan: sale.staff_loan, status_id: sale.status_id, user_id: sale.user_id }
    assert_redirected_to sale_path(assigns(:sale))
  end

  def test_destroy
    assert_difference("Sale.count", -1) do
      delete :destroy, id: sale
    end

    assert_redirected_to sales_path
  end
end
