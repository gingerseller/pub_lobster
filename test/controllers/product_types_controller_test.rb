require "test_helper"

class ProductTypesControllerTest < ActionController::TestCase
  def product_type
    @product_type ||= product_types :one
  end

  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:product_types)
  end

  def test_new
    get :new
    assert_response :success
  end

  def test_create
    assert_difference("ProductType.count") do
      post :create, product_type: { description: product_type.description, extra_land_price: product_type.extra_land_price, extra_land_square_feet: product_type.extra_land_square_feet, extra_land_square_meter: product_type.extra_land_square_meter, land_area_square_feet: product_type.land_area_square_feet, land_area_square_meter: product_type.land_area_square_meter, name: product_type.name, product_id: product_type.product_id }
    end

    assert_redirected_to product_type_path(assigns(:product_type))
  end

  def test_show
    get :show, id: product_type
    assert_response :success
  end

  def test_edit
    get :edit, id: product_type
    assert_response :success
  end

  def test_update
    put :update, id: product_type, product_type: { description: product_type.description, extra_land_price: product_type.extra_land_price, extra_land_square_feet: product_type.extra_land_square_feet, extra_land_square_meter: product_type.extra_land_square_meter, land_area_square_feet: product_type.land_area_square_feet, land_area_square_meter: product_type.land_area_square_meter, name: product_type.name, product_id: product_type.product_id }
    assert_redirected_to product_type_path(assigns(:product_type))
  end

  def test_destroy
    assert_difference("ProductType.count", -1) do
      delete :destroy, id: product_type
    end

    assert_redirected_to product_types_path
  end
end
