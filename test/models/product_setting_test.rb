# == Schema Information
#
# Table name: product_settings
#
#  id                                :integer          not null, primary key
#  product_id                        :integer
#  allow_multiper_booking            :boolean          default(FALSE)
#  unit_per_row                      :integer
#  notify_buyer_on_sale_confirmation :boolean          default(FALSE)
#  notify_admin_on_sale_confirmation :boolean          default(FALSE)
#  bumiputera_discount               :integer
#  attach_payment_image              :boolean          default(FALSE)
#  created_at                        :datetime         not null
#  updated_at                        :datetime         not null
#

require "test_helper"

class ProductSettingTest < ActiveSupport::TestCase
  def product_setting
    @product_setting ||= ProductSetting.new
  end

  def test_valid
    assert product_setting.valid?
  end
end
