# == Schema Information
#
# Table name: email_settings
#
#  id         :integer          not null, primary key
#  product_id :integer
#  subject    :string
#  from       :string
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require "test_helper"

class EmailSettingTest < ActiveSupport::TestCase
  def email_setting
    @email_setting ||= EmailSetting.new
  end

  def test_valid
    assert email_setting.valid?
  end
end
