# == Schema Information
#
# Table name: projects
#
#  id           :integer          not null, primary key
#  name         :string
#  description  :text
#  status_id    :integer
#  company_id   :integer
#  is_published :boolean          default(FALSE)
#  slug         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  admin_id     :integer
#

require "test_helper"

class ProjectTest < ActiveSupport::TestCase
  def project
    @project ||= Project.new
  end

  def test_valid
    assert project.valid?
  end
end
