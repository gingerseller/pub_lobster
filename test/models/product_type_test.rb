# == Schema Information
#
# Table name: product_types
#
#  id                      :integer          not null, primary key
#  name                    :string
#  description             :text
#  product_id              :integer
#  land_area_square_meter  :integer
#  land_area_square_feet   :integer
#  extra_land_square_meter :integer
#  extra_land_square_feet  :integer
#  extra_land_price        :integer
#  slug                    :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

require "test_helper"

class ProductTypeTest < ActiveSupport::TestCase
  def product_type
    @product_type ||= ProductType.new
  end

  def test_valid
    assert product_type.valid?
  end
end
