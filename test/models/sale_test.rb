# == Schema Information
#
# Table name: sales
#
#  id                     :integer          not null, primary key
#  project_id             :integer
#  product_id             :integer
#  lot_unit_id            :integer
#  phase_id               :integer
#  user_id                :integer
#  status_id              :integer
#  buyer_id               :integer
#  downpayment            :integer
#  downpayment_percentage :integer
#  bank_loan              :string
#  spa                    :string
#  booking_fee            :integer
#  reject_reason          :string
#  downpayment_type       :string
#  confirm_date           :datetime
#  admin_confirm_user_id  :integer
#  cash                   :string
#  government_loan        :string
#  staff_loan             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

require "test_helper"

class SaleTest < ActiveSupport::TestCase
  def sale
    @sale ||= Sale.new
  end

  def test_valid
    assert sale.valid?
  end
end
