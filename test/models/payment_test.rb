# == Schema Information
#
# Table name: payments
#
#  id                 :integer          not null, primary key
#  sale_id            :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#

require "test_helper"

class PaymentTest < ActiveSupport::TestCase
  def payment
    @payment ||= Payment.new
  end

  def test_valid
    assert payment.valid?
  end
end
