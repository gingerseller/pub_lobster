# == Schema Information
#
# Table name: companies
#
#  id                  :integer          not null, primary key
#  name                :string
#  registration_number :string
#  address             :text
#  phone_number        :string
#  fax_number          :string
#  type_id             :integer
#  parent_id           :integer
#  status_id           :integer
#  slug                :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  logo_file_name      :string
#  logo_content_type   :string
#  logo_file_size      :integer
#  logo_updated_at     :datetime
#

require "test_helper"

class CompanyTest < ActiveSupport::TestCase
  def company
    @company ||= Company.new
  end

  def test_valid
    assert company.valid?
  end
end
