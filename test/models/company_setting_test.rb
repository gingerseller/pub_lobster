# == Schema Information
#
# Table name: company_settings
#
#  id                     :integer          not null, primary key
#  company_id             :integer
#  allow_multiple_booking :boolean          default(TRUE)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

require "test_helper"

class CompanySettingTest < ActiveSupport::TestCase
  def company_setting
    @company_setting ||= CompanySetting.new
  end

  def test_valid
    assert company_setting.valid?
  end
end
