# == Schema Information
#
# Table name: nationalities
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require "test_helper"

class NationalityTest < ActiveSupport::TestCase
  def nationality
    @nationality ||= Nationality.new
  end

  def test_valid
    assert nationality.valid?
  end
end
