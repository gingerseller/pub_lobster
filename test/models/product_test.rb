# == Schema Information
#
# Table name: products
#
#  id             :integer          not null, primary key
#  name           :string
#  type_id        :integer
#  description    :text
#  status_id      :integer
#  phase_id       :integer
#  is_published   :boolean          default(FALSE)
#  company_id     :integer
#  e_brochure_url :string
#  slug           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require "test_helper"

class ProductTest < ActiveSupport::TestCase
  def product
    @product ||= Product.new
  end

  def test_valid
    assert product.valid?
  end
end
