# == Schema Information
#
# Table name: regions
#
#  id         :integer          not null, primary key
#  name       :string
#  product_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require "test_helper"

class RegionTest < ActiveSupport::TestCase
  def region
    @region ||= Region.new
  end

  def test_valid
    assert region.valid?
  end
end
