# == Schema Information
#
# Table name: sources_types
#
#  id         :integer          not null, primary key
#  name       :string
#  product_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require "test_helper"

class SourcesTypeTest < ActiveSupport::TestCase
  def sources_type
    @sources_type ||= SourcesType.new
  end

  def test_valid
    assert sources_type.valid?
  end
end
