# == Schema Information
#
# Table name: floor_plans
#
#  id                 :integer          not null, primary key
#  product_id         :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#

require "test_helper"

class FloorPlanTest < ActiveSupport::TestCase
  def floor_plan
    @floor_plan ||= FloorPlan.new
  end

  def test_valid
    assert floor_plan.valid?
  end
end
