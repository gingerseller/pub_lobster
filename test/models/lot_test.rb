# == Schema Information
#
# Table name: lots
#
#  id                      :integer          not null, primary key
#  name                    :string
#  description             :text
#  product_id              :integer
#  product_type_id         :integer
#  status_id               :integer          default(1)
#  land_area_square_meter  :integer          default(0)
#  land_area_square_feet   :integer          default(0)
#  extra_land_square_meter :integer          default(0)
#  extra_land_square_feet  :integer          default(0)
#  premium                 :integer
#  extra_land_price        :integer          default(0)
#  selling_price           :integer          default(0)
#  row_key                 :integer          default(1)
#  is_special_unit         :boolean          default(FALSE)
#  slug                    :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

require "test_helper"

class LotTest < ActiveSupport::TestCase
  def lot
    @lot ||= Lot.new
  end

  def test_valid
    assert lot.valid?
  end
end
