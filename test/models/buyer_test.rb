# == Schema Information
#
# Table name: buyers
#
#  id                    :integer          not null, primary key
#  full_name             :string
#  ic_number             :string
#  address               :string
#  mobile_contact_number :string
#  home_contact_number   :string
#  email                 :string
#  gender                :string
#  is_bumi_putera        :boolean          default(FALSE)
#  source_type_id        :integer
#  race                  :integer
#  age                   :integer
#  region_id             :integer
#  buyer_second_name     :string
#  buyer_third_name      :string
#  second_ic_number      :string
#  third_ic_number       :integer
#  nationality_id        :integer
#  postcode              :integer
#  car_park              :string
#  remark                :text
#  slug                  :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

require "test_helper"

class BuyerTest < ActiveSupport::TestCase
  def buyer
    @buyer ||= Buyer.new
  end

  def test_valid
    assert buyer.valid?
  end
end
