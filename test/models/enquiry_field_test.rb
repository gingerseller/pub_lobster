# == Schema Information
#
# Table name: enquiry_fields
#
#  id         :integer          not null, primary key
#  enquiry_id :integer
#  name       :string
#  content    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require "test_helper"

class EnquiryFieldTest < ActiveSupport::TestCase
  def enquiry_field
    @enquiry_field ||= EnquiryField.new
  end

  def test_valid
    assert enquiry_field.valid?
  end
end
