# == Schema Information
#
# Table name: enquiries
#
#  id         :integer          not null, primary key
#  product_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require "test_helper"

class EnquiryTest < ActiveSupport::TestCase
  def enquiry
    @enquiry ||= Enquiry.new
  end

  def test_valid
    assert enquiry.valid?
  end
end
