# == Schema Information
#
# Table name: phases
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  status_id   :integer
#  project_id  :integer
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require "test_helper"

class PhaseTest < ActiveSupport::TestCase
  def phase
    @phase ||= Phase.new
  end

  def test_valid
    assert phase.valid?
  end
end
