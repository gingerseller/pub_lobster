# == Schema Information
#
# Table name: site_plans
#
#  id                 :integer          not null, primary key
#  product_id         :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#

require "test_helper"

class SitePlanTest < ActiveSupport::TestCase
  def site_plan
    @site_plan ||= SitePlan.new
  end

  def test_valid
    assert site_plan.valid?
  end
end
