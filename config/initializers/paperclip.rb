RACKSPACE_CONFIG = {
  'production' => {
    path: '',
    storage: :fog,
    fog_credentials: {
      provider: 'Rackspace',
      rackspace_username: 'estatekitkat',
      rackspace_api_key: '52cf536d0d1b1777371857d419659b73',
      rackspace_region: :hkg,
      persistent: false
    },
    fog_directory: 'public_lobster_production',
    fog_public: true,
    fog_host: 'http://9b5acbade58fb11e7679-d43146fb38b8860e59389f139b7dca6c.r28.cf6.rackcdn.com'
  },
  'staging' => {
    path: '',
    storage: :fog,
    fog_credentials: {
      provider: 'Rackspace',
      rackspace_username: 'estatekitkat',
      rackspace_api_key: '52cf536d0d1b1777371857d419659b73',
      rackspace_region: :hkg,
      persistent: false
    },
    fog_directory: 'public_lobster_development',
    fog_public: true,
    fog_host: 'http://68504c37d6123773e3c6-f5c6a100f6ae4cc1fa42b8c2cc928a04.r44.cf6.rackcdn.com'
  },
  'development' => {
    # path: '',
    # storage: :fog,
    # fog_credentials: {
    #   provider: 'Rackspace',
    #   rackspace_username: 'estatekitkat',
    #   rackspace_api_key: '52cf536d0d1b1777371857d419659b73',
    #   rackspace_region: :hkg,
    #   persistent: false
    # },
    # fog_directory: 'development',
    # fog_public: true,
    # fog_host: 'http://aad3c01d04d896c3c073-33259659036bd2cd15d862c52ee3d11b.r23.cf6.rackcdn.com'
    path: '',
    storage: :fog,
    fog_credentials: {
      provider: 'Local',
      local_root: "#{Rails.root}/public"
    },
    fog_directory: '',
    fog_host: 'http://localhost:3000'
  },
  'test' => {
    path: '',
    storage: :fog,
    fog_credentials: {
      provider: 'Local',
      local_root: "#{Rails.root}/public"
    },
    fog_directory: '',
    fog_host: 'http://localhost:3000'
  }
}

Paperclip::Attachment.default_options.update(RACKSPACE_CONFIG[Rails.env])